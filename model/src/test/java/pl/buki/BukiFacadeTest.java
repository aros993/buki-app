package pl.buki;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.buki.exceptions.CannotBuyBet;
import pl.buki.exceptions.CannotBuyBetOnStartedEvent;
import pl.buki.exceptions.NegativeFundAmount;
import pl.buki.exceptions.PlayerNotFound;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BukiFacadeTest {

    private BukiFacade bukiFacade;
    private BukiRepository bukiRepository;
    private SportEventsCatalogue sportEventsCatalogue;

    @BeforeEach
    void setup() {
        bukiRepository = mock(BukiRepository.class);
        sportEventsCatalogue = mock(SportEventsCatalogue.class);
        bukiFacade = new BukiFacade(bukiRepository, sportEventsCatalogue);
    }

    @Test
    void shouldFundPlayer() {
        //given
        final String playerId = "2222";
        final int euroGoals = 500;
        final Player player = new Player(playerId, 0);
        when(bukiRepository.findPlayer(anyString())).thenReturn(player);

        //when
        bukiFacade.fund(euroGoals, playerId);

        //then
        Assertions.assertThat(player.getEuroGoals()).isEqualTo(500);
    }

    @Test
    void shouldThrowNegativeEuroGoalsAmount() {
        //when
        Throwable thrown = catchThrowable(() -> bukiFacade.fund(-500, "2222"));

        //then
        assertThat(thrown).isInstanceOf(NegativeFundAmount.class);
    }

    @Test
    void shouldThrowPlayerNotFound() {
        //given
        when(bukiRepository.findPlayer(anyString())).thenReturn(null);

        //when
        Throwable thrown = catchThrowable(() -> bukiFacade.fund(500, "2222"));

        //then
        assertThat(thrown).isInstanceOf(PlayerNotFound.class);
    }

    @Test
    void shouldBuySportEvent() {
        //given
        final String playerId = "2222";
        final Player player = new Player(playerId, 100);
        when(bukiRepository.findPlayer(anyString())).thenReturn(player);
        when(sportEventsCatalogue.find(anySet())).thenReturn(Arrays.asList(new SportEvent(UUID.randomUUID(), LocalDateTime.now().plusHours(2)),
                new SportEvent(UUID.randomUUID(), LocalDateTime.now().plusHours(2)),
                new SportEvent(UUID.randomUUID(), LocalDateTime.now().plusHours(2))));

        final int amount = 50;

        final Map<UUID, SportEventBet.SportBetVariant> sportEvents = new HashMap<>();
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.GUESTS_WIN);
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.HOSTS_WIN);
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.REMIS);

        //when
        bukiFacade.buy(new MakeSportBetCommand(playerId, amount, sportEvents));

        //then
        Assertions.assertThat(player.getSportBets()).hasSize(1);
        assertThat(player.getEuroGoals()).isEqualTo(50);
    }

    @Test
    void shouldThrowCannotBuyBetOnStartedEventException() {
        //given
        final String playerId = "2222";
        final Player player = new Player(playerId, 100);
        when(bukiRepository.findPlayer(anyString())).thenReturn(player);
        when(sportEventsCatalogue.find(anySet())).thenReturn(Arrays.asList(new SportEvent(UUID.randomUUID(), LocalDateTime.now().plusHours(2)),
                new SportEvent(UUID.randomUUID(), LocalDateTime.now().plusHours(2)),
                new SportEvent(UUID.randomUUID(), LocalDateTime.now().minusHours(2))));

        final int amount = 50;

        final Map<UUID, SportEventBet.SportBetVariant> sportEvents = new HashMap<>();
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.GUESTS_WIN);
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.HOSTS_WIN);

        //when
        Throwable thrown = catchThrowable(() -> bukiFacade.buy(new MakeSportBetCommand(playerId, amount, sportEvents)));

        //then
        assertThat(thrown).isInstanceOf(CannotBuyBetOnStartedEvent.class);
    }

    @Test
    void shouldThrowCannotBuyBetIfPlayerGoalsIsLessThanBetAmount() {
        //given
        final String playerId = "2222";
        final Player player = new Player(playerId, 100);
        when(bukiRepository.findPlayer(anyString())).thenReturn(player);

        final int amount = 150;

        final Map<UUID, SportEventBet.SportBetVariant> sportEvents = new HashMap<>();
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.GUESTS_WIN);
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.HOSTS_WIN);

        //when
        Throwable thrown = catchThrowable(() -> bukiFacade.buy(new MakeSportBetCommand(playerId, amount, sportEvents)));

        //then
        assertThat(thrown).isInstanceOf(CannotBuyBet.class);
    }
}

package pl.buki

import pl.buki.exceptions.CannotBuyBet
import pl.buki.exceptions.CannotBuyBetOnStartedEvent
import pl.buki.exceptions.NegativeFundAmount
import pl.buki.exceptions.PlayerNotFound
import spock.lang.Specification

import java.time.LocalDateTime

import static org.mockito.ArgumentMatchers.anySet
import static org.mockito.ArgumentMatchers.anyString
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

class BukiFacadeTestSpock extends Specification{
    def bukiRepository
    def sportEventsCatalogue
    def bukiFacade

    def setup() {
        bukiRepository = mock(BukiRepository.class);
        sportEventsCatalogue = mock(SportEventsCatalogue.class);
        bukiFacade = new BukiFacade(bukiRepository, sportEventsCatalogue);
    }

    def "should fund player"(){

        given:
        String playerId = "2222"
        Player player = new Player(playerId, 0);
        when(bukiRepository.findPlayer(anyString())).thenReturn(player)

        when:
        bukiFacade.fund(euroGoals, playerId);

        then:
        player.euroGoals == goals

        where:
        euroGoals | goals
        500 | 500
        300 | 300
        10000 | 10000
    }

    def "should thrown negative euro goals amount"(){
        when:
        bukiFacade.fund(-500, "2222")

        then:
        thrown NegativeFundAmount
    }

    def "should thrown player not found"(){
        given:
        when(bukiRepository.findPlayer(anyString())).thenReturn(null)

        when:
        bukiFacade.fund(500, "2222")

        then:
        thrown PlayerNotFound
    }

    def "should buy sport event"(){

        given:
        String playerId = "2222";
        Player player = new Player(playerId, playerAmount);
        when(bukiRepository.findPlayer(anyString())).thenReturn(player)
        when(sportEventsCatalogue.find(anySet())).thenReturn(Arrays.asList(new SportEvent(UUID.randomUUID(), LocalDateTime.now().plusHours(2)),
                new SportEvent(UUID.randomUUID(), LocalDateTime.now().plusHours(2)),
                new SportEvent(UUID.randomUUID(), LocalDateTime.now().plusHours(2))))

        Map<UUID, SportEventBet.SportBetVariant> sportEvents = new HashMap<>();
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.GUESTS_WIN);
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.HOSTS_WIN);
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.REMIS);

        when:
        bukiFacade.buy(new MakeSportBetCommand(playerId, betAmount, sportEvents));

        then:
        player.sportBets.size() == 1
        player.euroGoals == amountAfter

        where:
        playerAmount | betAmount || amountAfter
        100 | 50 || 50
        500 | 100 || 400
        300 | 1 || 299
    }

    def "should thrown CannotBuyBetOnStartedEvent exception"(){
        given:
        String playerId = "2222";
        Player player = new Player(playerId, 100);
        when(bukiRepository.findPlayer(anyString())).thenReturn(player);
        when(sportEventsCatalogue.find(anySet())).thenReturn(Arrays.asList(new SportEvent(UUID.randomUUID(), LocalDateTime.now().plusHours(2)),
                new SportEvent(UUID.randomUUID(), LocalDateTime.now().plusHours(2)),
                new SportEvent(UUID.randomUUID(), LocalDateTime.now().minusHours(2))));

        Map<UUID, SportEventBet.SportBetVariant> sportEvents = new HashMap<>();
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.GUESTS_WIN);
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.HOSTS_WIN);

        when:
        bukiFacade.buy(new MakeSportBetCommand(playerId, 50, sportEvents))

        then:
        thrown CannotBuyBetOnStartedEvent

    }

    def "should thrown CannotBuyBet if player goals is less than bet amount"(){
        given:
        String playerId = "2222";
        Player player = new Player(playerId, 100);
        when(bukiRepository.findPlayer(anyString())).thenReturn(player);

        int amount = 150;

        Map<UUID, SportEventBet.SportBetVariant> sportEvents = new HashMap<>();
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.GUESTS_WIN);
        sportEvents.put(UUID.randomUUID(), SportEventBet.SportBetVariant.HOSTS_WIN);

        when:
        bukiFacade.buy(new MakeSportBetCommand(playerId, amount, sportEvents))

        then:
        thrown CannotBuyBet

    }

}

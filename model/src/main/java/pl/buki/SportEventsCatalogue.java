package pl.buki;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface SportEventsCatalogue {

    List<SportEvent> find(final Set<UUID> ids);
}

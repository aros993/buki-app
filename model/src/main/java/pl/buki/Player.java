package pl.buki;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

class Player {

    private final String id;
    private int amount;

    private List<SportBet> sportBets = new ArrayList<>();

    Player(final String id, final int amount) {
        this.id = id;
        this.amount = amount;
    }

    String getId() {
        return id;
    }

    int getEuroGoals() {
        return amount;
    }

    List<SportBet> getSportBets() {
        return new ArrayList<>(sportBets);
    }

    void addSportBet(SportBet sportBet) {
        sportBets.add(sportBet);
    }

    void fund(final int amount) {
        this.amount += amount;
    }

    boolean canBuy(final int amount) {
        return this.amount >= amount;
    }

    void balanceAmount(final int amount) {
        if(this.canBuy(amount)) {
            this.amount -= amount;
        }
    }

    void buyBet(MakeSportBetCommand sportBetCommand, List<SportEvent> sportEvents) {
        final SportBet sportBet = new SportBet(UUID.randomUUID(), sportBetCommand.getAmount());
        sportBet.addEventBets(createSportEventBetList(sportBetCommand, sportEvents));
        addSportBet(sportBet);

        balanceAmount(sportBetCommand.getAmount());
    }

    private List<SportEventBet> createSportEventBetList(MakeSportBetCommand sportBetCommand, List<SportEvent> sportEvents) {
        return sportEvents.stream().map(s -> new SportEventBet(s, sportBetCommand.getSportEventVariant(s.getId()))).collect(Collectors.toList());
    }
}

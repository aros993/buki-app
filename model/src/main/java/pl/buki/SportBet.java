package pl.buki;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SportBet {

    private final UUID id;
    private final int amount;
    private final List<SportEventBet> sportEventBets = new ArrayList<>();

    public SportBet(UUID id, final int amount) {
        this.id = id;
        this.amount = amount;
    }

    public void addEventBets(final List<SportEventBet> sportEventBets) {
        sportEventBets.addAll(sportEventBets);
    }
}

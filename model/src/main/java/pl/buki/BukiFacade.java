package pl.buki;

import pl.buki.exceptions.CannotBuyBet;
import pl.buki.exceptions.CannotBuyBetOnStartedEvent;
import pl.buki.exceptions.NegativeFundAmount;
import pl.buki.exceptions.PlayerNotFound;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class BukiFacade {

    private BukiRepository bukiRepository;
    private SportEventsCatalogue sportEventsCatalogue;

    BukiFacade(final BukiRepository bukiRepository, final SportEventsCatalogue sportEventsCatalogue) {
        this.bukiRepository = bukiRepository;
        this.sportEventsCatalogue = sportEventsCatalogue;
    }

    void fund(int euroGoals, String playerId) {
        if(euroGoals <= 0) {
            throw new NegativeFundAmount(euroGoals);
        }

        Player player = bukiRepository.findPlayer(playerId);

        if(Objects.isNull(player)) {
            throw new PlayerNotFound();
        }

        player.fund(euroGoals);

        bukiRepository.save(player);
    }

    void buy(final MakeSportBetCommand sportBetCommand) {
        Player player = bukiRepository.findPlayer(sportBetCommand.getPlayerId());

        if(Objects.isNull(player)) {
            throw new PlayerNotFound();
        }

        if(!player.canBuy(sportBetCommand.getAmount())) {
            throw new CannotBuyBet();
        }

        List<SportEvent> sportEvents = sportEventsCatalogue.find(sportBetCommand.getSportsEvents());

        if(!sportEvents.stream().allMatch(SportEvent::canBet)) {
            throw new CannotBuyBetOnStartedEvent();
        }

        player.buyBet(sportBetCommand, sportEvents);

        bukiRepository.save(player);
    }


}

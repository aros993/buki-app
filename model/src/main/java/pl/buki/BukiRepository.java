package pl.buki;

public interface BukiRepository {

    Player findPlayer(String playerId);
    void save(Player player);
}

package pl.buki;

import java.time.LocalDateTime;
import java.util.UUID;

public class SportEvent {

    private final UUID id;
    private final LocalDateTime startDate;

    public SportEvent(UUID id, LocalDateTime startDate) {
        this.id = id;
        this.startDate = startDate;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public UUID getId() {
        return id;
    }

    public boolean canBet() {
        return this.getStartDate().isAfter(LocalDateTime.now());
    }
}

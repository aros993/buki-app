package pl.buki;

public class SportEventBet {

    enum SportBetVariant {
        GUESTS_WIN,
        HOSTS_WIN,
        REMIS
    }

    private final SportEvent sportEvent;
    private final SportBetVariant sportBetVariant;

    public SportEventBet(SportEvent sportEvent, SportBetVariant sportBetVariant) {
        this.sportEvent = sportEvent;
        this.sportBetVariant = sportBetVariant;
    }
}

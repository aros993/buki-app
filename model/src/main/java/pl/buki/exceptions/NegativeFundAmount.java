package pl.buki.exceptions;

public class NegativeFundAmount extends RuntimeException {

    public NegativeFundAmount(final int euroGoals) {
        super("Euro Goals amount:" + euroGoals + " is less or equal than 0.");
    }
}

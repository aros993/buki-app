package pl.buki;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class MakeSportBetCommand {

    private final String playerId;
    private final int amount;
    private final Map<UUID, SportEventBet.SportBetVariant> sportEvents;

    public MakeSportBetCommand(String playerId, final int amount, Map<UUID, SportEventBet.SportBetVariant> sportEvents) {
        this.playerId = playerId;
        this.amount = amount;
        this.sportEvents = sportEvents;
    }

    public int getAmount() {
        return amount;
    }

    public String getPlayerId() {
        return playerId;
    }

    public Set<UUID> getSportsEvents() {
        return sportEvents.keySet();
    }

    public SportEventBet.SportBetVariant getSportEventVariant(UUID sportEventId) {
        return sportEvents.get(sportEventId);
    }
}

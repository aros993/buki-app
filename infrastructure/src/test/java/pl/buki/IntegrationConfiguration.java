package pl.buki;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
class IntegrationConfiguration {

    @Bean
    BukiClient bukiClient(TestRestTemplate testRestTemplate) {
        return new BukiClient(testRestTemplate);
    }
}

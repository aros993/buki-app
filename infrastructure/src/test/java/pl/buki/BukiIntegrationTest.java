package pl.buki;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BukiIntegrationTest {

    @Autowired
    private BukiClient bukiClient;

    @Test
    void shouldFundPlayer() {
        //given
        final String playerId = "1";
        final int euroGoalsAmount = 300;

        //when
        ResponseEntity<Void> response = bukiClient.fundPlayer(playerId, euroGoalsAmount);

        //then
        assertThat(response.getStatusCode()).isEqualByComparingTo(CREATED);
    }

    @Test
    void shouldNotFoundPlayer() {
        //given
        final String playerId = "1000";
        final int euroGoalsAmount = 300;

        //when
        ResponseEntity<Void> response = bukiClient.fundPlayer(playerId, euroGoalsAmount);

        //then
        assertThat(response.getStatusCode()).isEqualByComparingTo(NOT_FOUND);
    }

    @Test
    void shouldBeNegativeEuroGoalsAmount() {
        //given
        final String playerId = "1";
        final int euroGoalsAmount = -300;

        //when
        ResponseEntity<Void> response = bukiClient.fundPlayer(playerId, euroGoalsAmount);

        //then
        assertThat(response.getStatusCode()).isEqualByComparingTo(CONFLICT);
    }

    @Test
    void shouldButTicket() {
        String playerId = "1";
        Map<UUID, SportEventBet.SportBetVariant> sportEvents = new HashMap<>();
        sportEvents.put(UUID.fromString("f9f2cbcd-cbc9-4fca-8f2a-2e2cedbc0a87"), SportEventBet.SportBetVariant.GUESTS_WIN);
        sportEvents.put(UUID.fromString("7fe3dc5c-3cdf-4f7f-9406-32966611d358"), SportEventBet.SportBetVariant.GUESTS_WIN);
        sportEvents.put(UUID.fromString("4946c294-ec5f-4581-93eb-e66ab693d3c3"), SportEventBet.SportBetVariant.GUESTS_WIN);

        int amount = 50;

        ResponseEntity<Void> response = bukiClient.buyBet(playerId, amount, sportEvents);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

    }
}

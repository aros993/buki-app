package pl.buki;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.HttpMethod.POST;

class BukiClient {

    private final TestRestTemplate testRestTemplate;

    BukiClient(TestRestTemplate testRestTemplate) {
        this.testRestTemplate = testRestTemplate;
    }

    ResponseEntity<Void> fundPlayer(final String playerId, final int euroGoalsAmount) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{\n"
                + "  \"euroGoals\": "+euroGoalsAmount+"\n"
                + "}", headers);
        return testRestTemplate.exchange("/players/"+playerId+"/funds", POST, entity, Void.class);
    }

    ResponseEntity<Void> buyBet(final String playerId, final int euroGoalsAmount, Map<UUID, SportEventBet.SportBetVariant> sportEvents) {
        String events = sportEvents.entrySet().stream().map(e -> "\"" + e.getKey() + "\" : \"" + e.getValue() + "\"").collect(Collectors.joining(","));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{\n" +
                "  \"amount\": " + euroGoalsAmount + ",\n" +
                "  \"sportEvents\" : {\n" +
                events +
                "  }\n" +
                "}", headers);
        return testRestTemplate.exchange("/players/"+playerId+"/buy-bet", POST, entity, Void.class);
    }

}

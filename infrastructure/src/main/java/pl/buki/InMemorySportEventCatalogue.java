package pl.buki;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class InMemorySportEventCatalogue implements SportEventsCatalogue {

    private Map<UUID, SportEvent> sportEvents = new HashMap<>();

    public InMemorySportEventCatalogue() {
        sportEvents.put(UUID.fromString("f9f2cbcd-cbc9-4fca-8f2a-2e2cedbc0a87"), new SportEvent(UUID.fromString("f9f2cbcd-cbc9-4fca-8f2a-2e2cedbc0a87"), LocalDateTime.now().plusHours(2)));
        sportEvents.put(UUID.fromString("7fe3dc5c-3cdf-4f7f-9406-32966611d358"), new SportEvent(UUID.fromString("7fe3dc5c-3cdf-4f7f-9406-32966611d358"), LocalDateTime.now().plusHours(3)));
        sportEvents.put(UUID.fromString("4946c294-ec5f-4581-93eb-e66ab693d3c3"), new SportEvent(UUID.fromString("4946c294-ec5f-4581-93eb-e66ab693d3c3"), LocalDateTime.now().plusHours(1)));
    }

    @Override
    public List<SportEvent> find(Set<UUID> ids) {
        return ids.stream().map(sportEvents::get).collect(Collectors.toList());
    }
}

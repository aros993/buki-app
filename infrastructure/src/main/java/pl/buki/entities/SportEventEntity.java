package pl.buki.entities;

import java.time.LocalDateTime;
import java.util.UUID;

class SportEventEntity {

    private long entityId;

    private UUID id ;
    private String name;
    private String hostsTeam;
    private String guestsTeam;
    private LocalDateTime startDate;
}

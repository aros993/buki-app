package pl.buki;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.buki.exceptions.NegativeFundAmount;
import pl.buki.exceptions.PlayerNotFound;

import java.util.Map;
import java.util.UUID;

@RestController
class BukiRestController {

    private static final Logger log = LoggerFactory.getLogger(BukiRestController.class);

    static class Request {
        private int euroGoals;

        public int getEuroGoals() {
            return euroGoals;
        }

        public void setEuroGoals(int euroGoals) {
            this.euroGoals = euroGoals;
        }
    }

    static class BuyBetRequest{
        private int amount;
        private Map<UUID, SportEventBet.SportBetVariant> sportEvents;

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public Map<UUID, SportEventBet.SportBetVariant> getSportEvents() {
            return sportEvents;
        }

        public void setSportEvents(Map<UUID, SportEventBet.SportBetVariant> sportEvents) {
            this.sportEvents = sportEvents;
        }
    }

    private final BukiFacade bukiFacade;

    BukiRestController(BukiFacade bukiFacade) {
        this.bukiFacade = bukiFacade;
    }

    @PostMapping(path = "/players/{playerId}/funds", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    void fundPlayer(@PathVariable String playerId, @RequestBody Request request) {
        bukiFacade.fund(request.getEuroGoals(), playerId);
    }

    @PostMapping(path = "/players/{playerId}/buy-bet", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    void buyBet(@PathVariable String playerId, @RequestBody BuyBetRequest buyBetRequest){
        bukiFacade.buy(new MakeSportBetCommand(playerId, buyBetRequest.getAmount(), buyBetRequest.getSportEvents()));
    }

    @ExceptionHandler(value = { PlayerNotFound.class })
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    void handlePlayerNotFound(PlayerNotFound playerNotFound) {
        log.debug(playerNotFound.getMessage());
    }

    @ExceptionHandler(value = { NegativeFundAmount.class })
    @ResponseStatus(value = HttpStatus.CONFLICT)
    void handleNegativeEuroGoalsAmount(NegativeFundAmount negativeFundAmount) {
        log.debug(negativeFundAmount.getMessage());
    }
}

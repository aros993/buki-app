package pl.buki;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@EnableAsync
class BukiApplicationConfiguration {

    @Bean
    BukiFacade bukiFacade() {
        InMemoryBukiRepository inMemoryBukiRepository = new InMemoryBukiRepository();
        InMemorySportEventCatalogue inMemorySportEventCatalogue = new InMemorySportEventCatalogue();

        return new BukiFacade(inMemoryBukiRepository, inMemorySportEventCatalogue);
    }


}

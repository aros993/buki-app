package pl.buki;

import java.util.HashMap;
import java.util.Map;

public class InMemoryBukiRepository implements BukiRepository {

    private final Map<String, Player> players;

    public InMemoryBukiRepository() {
        this.players = new HashMap<>();

        this.players.put("1", new Player("1", 100));
        this.players.put("2", new Player("2", 200));
    }

    @Override
    public Player findPlayer(String playerId) {
        return players.get(playerId);
    }

    @Override
    public void save(Player player) {
        players.put(player.getId(), player);
    }
}
